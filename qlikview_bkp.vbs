Dim objShell
Dim objStartFolder
Dim script_path
Dim script_path_bkp
Dim objFolder
Dim colFiles

Set objShell = WScript.CreateObject( "WScript.Shell" )
Set objFSO = CreateObject("Scripting.FileSystemObject")

'get vbscript folder
objStartFolder = objFSO.GetParentFolderName(wscript.ScriptFullName) 
Set objFolder = objFSO.GetFolder(objStartFolder)

'create a folder to place extracted qlikview scripts
script_path_bkp = objStartFolder & "\backup_scripts_qlikview"
If Not objFSO.FolderExists(script_path_bkp) Then
    objFSO.CreateFolder script_path_bkp
End If

'get files in folder
Set colFiles = objFolder.Files

For Each objFile in colFiles
    
    'check if file is qlikview extension
    if UCase(Right(objFile.Name, 3)) = "QVW" then

        script_path = Left(objFile.Path, Len(objFile.Path) - 4) & "-prj"
        
        'create -prj folder
        If Not objFSO.FolderExists(script_path) Then
            objFSO.CreateFolder script_path
        End If
    
        'open and saves the qlikview document
        Set MyApp = CreateObject("QlikTech.QlikView")
        Set MyDoc = MyApp.OpenDocEx(objFile.Path, 2 , false , "" , "", "" , false)
        WScript.Sleep 2000
        MyDoc.Save
        WScript.Sleep 2000

        'extract script file form -prj folder
        If objFSO.FileExists(script_path & "\LoadScript.txt") Then 
            objFSO.CopyFile script_path & "\LoadScript.txt", script_path_bkp & "\" & left(objFile.Name, len(objFile.Name) - 4) & ".qvs", True
        End If 

        MyDoc.GetApplication.Quit
        
        'remove -prj folder
        If objFSO.FolderExists(script_path) Then  
            objFSO.DeleteFolder script_path
        End If 

    end if
Next

WScript.Echo "End of Backup, check folder: " & script_path_bkp
