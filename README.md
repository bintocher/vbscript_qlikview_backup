# VBScript Qlikview Scripts Backup

## About
**QlikView** files contains script, data and visualization. As the data usualy 
comes from a external source, the key piece to have backups is the script.
Creating backups of .qvw files will lead to huge storage and no way to use tools
as GIT.
This **VBScript** extract the script file from the .qvw file using **QlikView** 
-prj method.

## How to use
Place this script in the folder containing .qvw files and run the script.
It is required to have **QlikView** installed to work.